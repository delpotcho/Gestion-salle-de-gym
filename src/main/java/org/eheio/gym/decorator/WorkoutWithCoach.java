package org.eheio.gym.decorator;

public abstract class WorkoutWithCoach implements Workout {
	protected Workout workout;
	protected WorkoutWithCoach (Workout workout)
	{
		this.workout= workout;
	}
	@Override
	public void booking() {
		
	}
}
