package org.eheio.gym.dao;

import org.eheio.gym.entities.SubActivity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubActivityRepository extends JpaRepository<SubActivity,Long>{

}
