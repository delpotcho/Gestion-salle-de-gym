package org.eheio.gym.entities;

import java.util.Date;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@MappedSuperclass
@Data
public abstract class User {

	@Size(min = 3, max = 50)
	private String name;
	
	private String username;
	
	@NotBlank
	@Size(min = 3, max = 30)
	private String password;
	
	
	private String gender;
	
	private String cni;
	
	private int phone;
	
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@Email
	@NotBlank
	private String email;

}
