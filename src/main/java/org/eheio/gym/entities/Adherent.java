package org.eheio.gym.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Adherent extends User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String profession;
	@ManyToOne
	private Subscription subscription;
	@ManyToMany
	@JsonIgnore
	private Collection<Workout> workout;

	@Builder
	public Adherent(Long id, String name, String username, String cni, String email, int phone, String password) {
		this.setName(name);
		this.setUsername(username);
		this.setCni(cni);
		this.setEmail(email);
		this.setPhone(phone);
		this.setPassword(password);

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public Collection<Workout> getWorkout() {
		return workout;
	}

	public void setWorkout(Collection<Workout> workout) {
		this.workout = workout;
	}
	@Override
	public boolean equals(Object o) {
		
		return super.equals(o);
	}
	@Override
	public int hashCode() {
		
		return super.hashCode();
	}

}
