package org.eheio.gym.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.*;
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class Equipment {
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY)
	private  Long id ;
	private String name;
	private String typeEquipment ;
	private int numbre ;
	
	@ManyToMany(mappedBy = "equipment")
	private Collection<Hall> hall;

}
