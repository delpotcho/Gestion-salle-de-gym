package org.eheio.gym.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Coach extends User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToMany(mappedBy = "coach")
	@JsonIgnore
	private Collection<Workout> workout;

	@Builder
	public Coach(Long id, String name, String username, String cni, String email, int phone) {
		this.setName(name);
		this.setUsername(username);
		this.setCni(cni);
		this.setEmail(email);
		this.setPhone(phone);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Collection<Workout> getWorkout() {
		return workout;
	}

	public void setWorkout(Collection<Workout> workout) {
		this.workout = workout;
	}

}
