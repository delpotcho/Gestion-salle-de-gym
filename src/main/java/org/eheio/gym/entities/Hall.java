package org.eheio.gym.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Hall {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private int capacity;
	@ManyToMany
	@JoinTable(name = "hall_equipment", joinColumns = @JoinColumn(name = "hall_id"), inverseJoinColumns = @JoinColumn(name = "equipment_id"))
	private Collection<Equipment> equipment;
	@OneToMany(mappedBy = "hall")
	@JsonIgnore
	private Collection<Workout> workout;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Collection<Equipment> getEquipment() {
		return equipment;
	}

	public void setEquipment(Collection<Equipment> equipment) {
		this.equipment = equipment;
	}

	public Collection<Workout> getWorkout() {
		return workout;
	}

	public void setWorkout(Collection<Workout> workout) {
		this.workout = workout;
	}

	

}
