package org.eheio.gym.controller;

import java.util.List;

import org.eheio.gym.entities.Hall;
import org.eheio.gym.service.ServiceHall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hall")
public class HallController {

	@Autowired
	private ServiceHall serviceHall;
	
	@RequestMapping
	@GetMapping("/")
	public List<Hall> getHalls(){
		return serviceHall.getAllHall();
	}
	@RequestMapping("/new")
	@PostMapping
	public ResponseEntity<Object> newHall(@RequestBody Hall hall){
		serviceHall.createHall(hall);
		return new ResponseEntity<Object>("bien ajouter",HttpStatus.CREATED);
	}
}
