package org.eheio.gym.controller;

import java.util.Date;
import java.util.List;

import org.eheio.gym.dao.WorkoutRepository;
import org.eheio.gym.entities.Workout;
import org.eheio.gym.service.ServiceCoach;
import org.eheio.gym.service.ServiceHall;
import org.eheio.gym.service.ServiceWorkout;
import org.eheio.gym.service.activity.IActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/workout")
public class WorkoutController {
	@Autowired
	private ServiceWorkout serviceWorkout;
	@GetMapping()
	@RequestMapping("/")
	public List<Workout> getListWorkout() {
		return serviceWorkout.getAllWorkout();
	}
	
	@RequestMapping("/{id}")
	public Workout getWorkout(@PathVariable("id") Long id) {
		return serviceWorkout.getWorkoutById(id);
	}

	@PostMapping("/new")
	public ResponseEntity<Object> addWorkout(@RequestBody Workout workout) {
		JSONObject json = new JSONObject();
		try {
			serviceWorkout.createWorkout(workout);
			json.put("message", "workout bien ajouté");
			return new ResponseEntity<>(json,HttpStatus.CREATED);
		}catch (Exception e) {
			
			json.put("message", "workout non ajouté");
			return new ResponseEntity<>(json,HttpStatus.NOT_ACCEPTABLE);
		}
	
		
	}
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> addWorkout(@RequestBody Workout newWorkout,@PathVariable("id") Long id ) {
		JSONObject json = new JSONObject();
		try {
			Workout workout=serviceWorkout.getWorkoutById(id);
			if(workout!=null) {
				workout.setActivity(newWorkout.getActivity());
				workout.setCoach(newWorkout.getCoach());
				workout.setDate(newWorkout.getDate());
				workout.setDuration(newWorkout.getDuration());
				workout.setHall(newWorkout.getHall());
				workout.setLevel(newWorkout.getLevel());
				serviceWorkout.createWorkout(workout);
				json.put("message", "workout bien modifier");
				return new ResponseEntity<>(json,HttpStatus.CREATED);
			}
			json.put("message", "workout Not Found");
			return new ResponseEntity<>(json,HttpStatus.NOT_FOUND);
			
		}catch (Exception e) {
			json.put("message", "workout non modifier");
			return new ResponseEntity<>(json,HttpStatus.NOT_ACCEPTABLE);
		}
	
		
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteWorkout(@PathVariable("id") Long id) {
		JSONObject json = new JSONObject();
		try {
			Workout workout =  serviceWorkout.getWorkoutById(id);
			serviceWorkout.deleteWorkout(workout);
			json.put("message", "workout supprimer");
			return new ResponseEntity<>(json,HttpStatus.OK);
		}catch (Exception e) {
			json.put("message", "workout non supprimer");
			return new ResponseEntity<>(json,HttpStatus.NOT_ACCEPTABLE);
		}
		
	}
}
