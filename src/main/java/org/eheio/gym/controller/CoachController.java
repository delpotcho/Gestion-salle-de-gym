package org.eheio.gym.controller;

import java.util.Date;
import java.util.List;

import org.eheio.gym.entities.Adherent;
import org.eheio.gym.entities.Coach;
import org.eheio.gym.service.ServiceCoach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/coach")
public class CoachController {

	@Autowired
	ServiceCoach serviceCoach;
	@RequestMapping("/")
	public List<Coach> viewHomePage() {
	return	 serviceCoach.getAllCoach();

	}
	

	@GetMapping("/new")
	public ResponseEntity<Object> createCoach(@RequestBody Coach coach) {
		coach.setDate(new Date());
		 serviceCoach.createCoach(coach);
		
		return new ResponseEntity<>("message",HttpStatus.OK);
	}

	@RequestMapping("/{id}")
	public Coach createCoach(@PathVariable("id") Long id) {
		
		return serviceCoach.getCoachtById(id);
		
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteCoach(@PathVariable("id") Long id) {
		JSONObject json = new JSONObject();
		try {
			Coach coach =  serviceCoach.getCoachtById(id);
			serviceCoach.deleteCoach(coach);
			json.put("message", "Coach supprimer");
			return new ResponseEntity<>(json,HttpStatus.OK);
		}catch (Exception e) {
			json.put("message", "Coach non supprimer");
			return new ResponseEntity<>(json,HttpStatus.NOT_ACCEPTABLE);
		}
	}
}
