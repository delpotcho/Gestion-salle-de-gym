package org.eheio.gym.controller;

import java.util.Date;
import java.util.List;

import org.eheio.gym.entities.Adherent;
import org.eheio.gym.entities.Workout;
import org.eheio.gym.service.ServiceAdherent;
import org.eheio.gym.service.ServiceSubscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;
@RestController
@CrossOrigin("*")
@RequestMapping("/adherent")

public class AdherentController {
	@Autowired
	private ServiceAdherent serviceAdherent;
	  
	@RequestMapping("/" )
	@GetMapping
	public List<Adherent> viewHomePage() {
		return serviceAdherent.getAllAdherent();	
	}
	
	@PostMapping("/new")
	public ResponseEntity<Adherent> saveAdherent(@RequestBody Adherent adherent) {
		adherent.setDate(new Date());
		Adherent ad = serviceAdherent.createAdherent(adherent);
		return new ResponseEntity<>(ad,HttpStatus.OK);
	}
	
	@RequestMapping("/{id}")
	public Adherent createAdherant(@PathVariable("id") Long id) {
		
		return serviceAdherent.getAdherentById(id);
	}

	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteAdherent(@PathVariable("id") Long id) {
		JSONObject json = new JSONObject();
		try {
			Adherent adherent =   serviceAdherent.getAdherentById(id);
			serviceAdherent.deleteAdhernet(adherent);
			json.put("message", "Adherent supprimer");
			return new ResponseEntity<>(json,HttpStatus.OK);
		}catch (Exception e) {
			json.put("message", "Adherent non supprimer");
			return new ResponseEntity<>(json,HttpStatus.NOT_ACCEPTABLE);
		}
	}
}
