package org.eheio.gym.service.activity;

import java.util.List;

import org.eheio.gym.dao.ActivityRepository;
import org.eheio.gym.entities.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ActivityService implements IActivityService {
	
	@Autowired
	ActivityRepository activityRepository;
	
	@Override
	public void createActivity(Activity activity) {
		activityRepository.save(activity);
	}

	@Override
	public void updateActivity(Activity activity) {
		activityRepository.save(activity);
	}

	@Override
	public void deleteActivity(Activity activity) {
		activityRepository.delete(activity);
	}

	@Override
	public Activity getActivitylById(Long id) {
		return activityRepository.getOne(id);
	}

	@Override
	public List<Activity> getAllActivity() {
		return activityRepository.findAll();
	}

	@Override
	public List<Activity> getActivityByKeyWords(String key) {
		return activityRepository.findAll();
	}

}
