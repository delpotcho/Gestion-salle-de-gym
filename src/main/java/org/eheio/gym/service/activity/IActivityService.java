package org.eheio.gym.service.activity;

import java.util.List;

import org.eheio.gym.entities.Activity;

public interface IActivityService {
	
	public void createActivity(Activity activity);

	public void updateActivity(Activity activity);

	public void deleteActivity(Activity activity);

	public Activity getActivitylById(Long id);

	public List<Activity> getAllActivity();

	public List<Activity> getActivityByKeyWords(String key);

}
