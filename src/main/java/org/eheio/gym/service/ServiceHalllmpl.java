package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.dao.HallRepository;
import org.eheio.gym.entities.Hall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class ServiceHalllmpl implements ServiceHall {
	@Autowired
	HallRepository hallRepository ; 
	
	@Override
	public void createHall(Hall hall) {
	hallRepository.save(hall);
	}

	@Override
	public void updateHall(Hall hall) {
		hallRepository.save(hall);
		
	}

	@Override
	public void deleteHall(Hall hall) {
		hallRepository.delete(hall);
		
	}

	@Override
	public Hall getHallById(Long id) {
		
		return  hallRepository.getOne(id);
	}

	@Override
	public List<Hall> getAllHall() {
		
		return hallRepository.findAll();
	}

	@Override
	public List<Hall> getHallByKeyWords(String key) {
		
		return hallRepository.findAll();
	}

}
