package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.entities.Subscription;
import org.springframework.stereotype.Component;
@Component
public interface ServiceSubscription  {
	public  List<Subscription> getAllSbscription();

}
