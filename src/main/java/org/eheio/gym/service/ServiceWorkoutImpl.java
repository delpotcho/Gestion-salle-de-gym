package org.eheio.gym.service;

import java.util.List;
import java.util.Optional;

import org.eheio.gym.dao.WorkoutRepository;
import org.eheio.gym.entities.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceWorkoutImpl implements ServiceWorkout {
	@Autowired
	WorkoutRepository workoutRepository;

	@Override
	public void createWorkout(Workout workout) {
		workoutRepository.save(workout);

	}

	@Override
	public void updateWorkout(Workout workout) {
		workoutRepository.save(workout);

	}

	@Override
	public void deleteWorkout(Workout workout) {
		workoutRepository.delete(workout);
	}

	@Override
	public Workout getWorkoutById(Long id) {

		Optional<Workout> workouttOptional = workoutRepository.findById(id);

		if (!workouttOptional.isPresent()) {
			throw new RuntimeException("Workount not found" + id);
		}
		return workouttOptional.get();
	}

	@Override
	public List<Workout> getAllWorkout() {

		return workoutRepository.findAll();
	}

	@Override
	public List<Workout> getWorkoutByKeyWords(String key) {

		return null;
	}

}
