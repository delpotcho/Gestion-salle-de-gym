package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.entities.Workout;

public interface ServiceWorkout {
	
	public void createWorkout (Workout workout );
	
	public void updateWorkout (Workout workout );
	
	public void deleteWorkout (Workout workout);
	
	public Workout getWorkoutById (Long id );
	
	public  List<Workout> getAllWorkout();
	
	public  List <Workout> getWorkoutByKeyWords (String key);

}
