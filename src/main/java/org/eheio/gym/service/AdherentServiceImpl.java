package org.eheio.gym.service;

import java.util.List;
import java.util.Optional;

import org.eheio.gym.dao.AdherentRepository;
import org.eheio.gym.entities.Adherent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdherentServiceImpl implements ServiceAdherent {

	@Autowired
	AdherentRepository adherentRepository;

	@Override
	public Adherent createAdherent(Adherent adhrent) {

		return adherentRepository.save(adhrent);
	}

	@Override
	public void updateAdhernet(Adherent adhrent) {

		adherentRepository.save(adhrent);
	}

	@Override
	public void deleteAdhernet(Adherent adhrent) {

		adherentRepository.delete(adhrent);
	}

	@Override
	public Adherent getAdherentById(Long id) {

		Adherent adherent = null;
		Optional<Adherent> adherentOptional = adherentRepository.findById(id);

		if (!adherentOptional.isPresent()) {

			throw new RuntimeException("Adherent not Font : " + id);
		}
		adherent = adherentOptional.get();

		return adherent;
	}

	@Override
	public List<Adherent> getAllAdherent() {

		return adherentRepository.findAll();
	}

	@Override
	public List<Adherent> getAdherentByKeyWords(String key) {

		return null;

	}

}
