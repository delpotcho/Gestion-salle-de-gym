package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.entities.Adherent;


public interface ServiceAdherent {

	/**
	 * 
	 * @param adhrent
	 */
	public Adherent createAdherent(Adherent adhrent);

	/**
	 * 
	 * @param adhrent
	 */
	public void updateAdhernet(Adherent adhrent);

	/**
	 * 
	 * @param adhrent
	 */
	public void deleteAdhernet(Adherent adhrent);

	/**
	 * 
	 * @param id : 
	 * @return
	 */
	public Adherent getAdherentById(Long id);

	/**
	 * 
	 * @return
	 */
	public List<Adherent> getAllAdherent();

	/**
	 * 
	 * @param key
	 * @return
	 */
	public List<Adherent> getAdherentByKeyWords(String key);

}
