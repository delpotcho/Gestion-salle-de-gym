package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.dao.SubscriptionRepository;
import org.eheio.gym.entities.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceSubscriptionImpl implements ServiceSubscription  {
	
    @Autowired
	SubscriptionRepository sub ;
    
	@Override
	public List<Subscription> getAllSbscription() {
		
	return	sub.findAll();
	}

}
