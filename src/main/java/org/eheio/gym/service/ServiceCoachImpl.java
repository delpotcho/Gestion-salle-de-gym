package org.eheio.gym.service;

import java.util.List;
import java.util.Optional;

import org.eheio.gym.dao.CoachRepository;
import org.eheio.gym.entities.Coach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceCoachImpl implements ServiceCoach {
	@Autowired
	CoachRepository coachRepository;

	@Override
	public void createCoach(Coach coach) {
		coachRepository.save(coach);

	}

	@Override
	public void updateCoach(Coach coach) {
		coachRepository.save(coach);

	}

	@Override
	public void deleteCoach(Coach coach) {
		coachRepository.delete(coach);

	}

	@Override
	public Coach getCoachtById(Long id) {
		Optional<Coach> optionalCoach = coachRepository.findById(id);

		if (!optionalCoach.isPresent()) {
			throw new RuntimeException("Coach not Font" + id);
		}
		return optionalCoach.get();

	}

	@Override
	public List<Coach> getAllCoach() {

		return coachRepository.findAll();
	}

	@Override
	public List<Coach> getCoachByKeyWords(String key) {

		return null;
	}

}
