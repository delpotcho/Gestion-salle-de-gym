package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.entities.Hall;

public interface ServiceHall {
	public void createHall(Hall hall);

	public void updateHall(Hall hall);

	public void deleteHall(Hall hall);

	public Hall getHallById(Long id);

	public List<Hall> getAllHall();

	public List<Hall> getHallByKeyWords(String key);
}
