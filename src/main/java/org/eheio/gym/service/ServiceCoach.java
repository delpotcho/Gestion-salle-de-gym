package org.eheio.gym.service;

import java.util.List;

import org.eheio.gym.entities.Coach;


public interface ServiceCoach {

	public void createCoach(Coach coach);

	public void updateCoach(Coach coach);

	public void deleteCoach(Coach coach);

	public Coach getCoachtById(Long id);

	public List<Coach> getAllCoach();

	public List<Coach> getCoachByKeyWords(String key);
}
